import Foundation
import Firebase

class UserApi{
    func getUserInformation(uid: String,onSuccess: @escaping(User) -> Void)//->User{
    {
        let ref = Database.database().reference().child("users").child(uid)
        ref.observe(.value){ (snapshot) in
            if let dict = snapshot.value as? Dictionary<String,Any>{
                if let user = User.transformUser(dict: dict){
                    onSuccess(user)
                }
            }
        }
    }
    
    func getUserInforSingleEvent(uid: String, onSuccess: @escaping(User) -> Void) {
        let ref = Database.database().reference().child("users").child(uid)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? Dictionary<String, Any> {
                if let user = User.transformUser(dict: dict) {
                    onSuccess(user)
                }
            }
        }
    }
    
    func isOnline(bool: Bool){
        if !Auth.auth().currentUser!.uid.isEmpty{
            let ref =  Database.database().reference().child("users").child(Auth.auth().currentUser!.uid).child("isOnline")
            let dict: Dictionary<String,Any> = [
                "online": bool as Any,
                "latest": Date().timeIntervalSince1970 as Any
            ]
            ref.updateChildValues(dict)
        }
    }
}

