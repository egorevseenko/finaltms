
import Foundation
import Firebase

class InboxApi{
    func lastMessages(uid: String, onSuccess: @escaping(Inbox) -> Void){
        let ref = Database.database().reference().child("inbox").child(uid)
        ref.observe(DataEventType.childAdded){ (snapshot) in
            if let dict = snapshot.value as? Dictionary<String,Any>{
                Api.User.getUserInformation(uid: snapshot.key, onSuccess: { (user) in
                    if let inbox = Inbox.transformInbox(dict: dict, user: user){
                        onSuccess(inbox)}
                })
            }
            
        }
    }
}

