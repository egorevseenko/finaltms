import Foundation
struct Api {
    static var User = UserApi()
    static var Message = MessageApi()
    static var Inbox = InboxApi()
}

