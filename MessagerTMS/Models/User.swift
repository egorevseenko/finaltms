import Foundation
import Firebase

class User {
    var uid: String
    var username: String
    var email: String
    var profileImageUrl: String
    var profileImage = ""
   // var status: String
    
    var currentUserId: String {
        return Auth.auth().currentUser != nil ? Auth.auth().currentUser!.uid : ""
    }
    
    init(uid: String, username: String, email: String, profileImageUrl: String) {
        self.uid = uid
        self.username = username
        self.email = email
        self.profileImageUrl = profileImageUrl
        //self.status = status
    }

    //поскольку с firebase приходит словарь с параметрами каждого пользователя,то его необходимо приобразовать в объект класса User
    static func transformUser(dict: [String: Any]) -> User? {
        guard let email = dict["email"] as? String,
              let username = dict["username"] as? String,
              let profileImageUrl = dict["profileImageUrl"] as? String,
//              let status = dict["status"] as? String,
              let uid = dict["uid"] as? String else { return nil }
        let user = User(uid: uid, username: username, email: email, profileImageUrl: profileImageUrl)
        return user
    }
    
}

