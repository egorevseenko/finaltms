import Foundation
import Firebase

class MessageApi{
    func sendMessage(from: String, to: String, value: Dictionary<String, Any>){
        let ref = Database.database().reference().child("messages").child(from).child(to)
        ref.childByAutoId().updateChildValues(value)
        
        var dict = value
        if let text = dict["text"] as? String, text.isEmpty{
            dict["imageUrl"] = nil
//            dict["height"] = nil
//            dict["width"] = nil
        }

        
        let mesageFrom = Database.database().reference().child("inbox").child(from).child(to)
        mesageFrom.updateChildValues(dict)
        
        let messageTo = Database.database().reference().child("inbox").child(to).child(from)
        messageTo.updateChildValues(dict)
    }
    
    func receiveMessage(from: String, to: String, onSuccess: @escaping(Message) -> Void){
        let ref = Database.database().reference().child("messages").child(from).child(to)
        ref.observe(.childAdded){(snapshot) in
            if let dict = snapshot.value as? Dictionary<String, Any>{
                if let message = Message.transformMessage(dict: dict, keyId: snapshot.key){
                onSuccess(message)
                }
            }
        }
    }
}
