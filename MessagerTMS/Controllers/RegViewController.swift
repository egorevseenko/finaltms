import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import ProgressHUD


class RegViewController: UIViewController {
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var image: UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAvatar()
        setNavigationBarInfo()
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboardRecognizerClicked(_:)))
        view.addGestureRecognizer(tapRecognizer)
    }

    
    @objc func hideKeyboardRecognizerClicked(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func setNavigationBarInfo(){
        title = "Registration"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isHidden = false
    }
    
    func setupAvatar(){
        avatarImageView.clipsToBounds = true
        avatarImageView.isUserInteractionEnabled = true
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(presentPicker(_:)))
        avatarImageView.addGestureRecognizer(tapImage)
    }
    
    @objc func presentPicker(_ sender: Any){
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        picker.modalPresentationStyle = .fullScreen
        self.present(picker, animated: true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let frameInView = textField.convert(textField.frame, to: view)
        let screenHeight = UIScreen.main.bounds.size.height
        let keyboardHeight: CGFloat = 216
        let offset = (screenHeight - keyboardHeight) - (frameInView.height + frameInView.origin.y + 50)
        //        if offset < 0 {
        //            view.contentOffset.y = offset * (-1)
        //        }
    }
    
    @IBAction func eyeButtonTapped(_ sender: UIButton) {
        eyeButton.isSelected.toggle()
        passwordTextField.isSecureTextEntry.toggle()
    }
    
    @IBAction func registrationClicked(_ sender: Any) {
        guard let imageSelected = self.image else {
            print("no image")
            let alertVC = UIAlertController(title: "Choose a picture", message: "", preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Go to gallery", style: .default, handler: {_ in
                self.presentPicker(UIButton.self)
            })
            alertVC.addAction(action)
            let actionBack = UIAlertAction(title: "Back", style: .destructive, handler: nil)
            alertVC.addAction(actionBack)
            self.present(alertVC, animated: true, completion: nil)
            return
        }
        guard let imageData = imageSelected.jpegData(compressionQuality: 0.4) else { return } // качество картинки от 0.0 до 1.0
        
        if let email = emailTextField.text, let password = passwordTextField.text{
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    Api.User.isOnline(bool: true)
                    let travelVC = MainViewController()
                    let settingsVC = SettingsViewController()
                    settingsVC.username = self.fullNameTextField.text
                    self.navigationController?.pushViewController(travelVC, animated: true)
                }
                if let authData = user{
                    print(authData.user.email)
                    var dict: Dictionary <String, Any> = [
                        "uid" : authData.user.uid,
                        "email" : authData.user.email,
                        "username" : self.fullNameTextField.text,
                        "profileImageUrl" : ""
                    ]
                    
                    let storageRef = Storage.storage().reference(forURL: "gs://finaltms-779f0.appspot.com")
                    let storageProfileRef = storageRef.child("profile").child(authData.user.uid)
                    
                    let metadata = StorageMetadata()
                    metadata.contentType = "image/jpg"
                    storageProfileRef.putData(imageData, metadata: metadata) { (storageMetaData, error) in
                        if error != nil{
                            print(error?.localizedDescription)
                            return
                        }
                        storageProfileRef.downloadURL { (url, error) in
                            if let metaImageUrl = url?.absoluteString{
                                print(metaImageUrl)
                                dict["profileImageUrl"] = metaImageUrl
                                
                                Database.database().reference().child("users").child(authData.user.uid).updateChildValues(dict) { (error, ref) in
                                    if error == nil {
                                        print("Done")
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    let errorMessage = error?.localizedDescription ?? "Error"
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertVC.addAction(action)
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
}

extension RegViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageSelected = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            image = imageSelected
            avatarImageView.image = imageSelected
        }
        if let imageOriginal = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            image = imageOriginal
            avatarImageView.image = imageOriginal
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
