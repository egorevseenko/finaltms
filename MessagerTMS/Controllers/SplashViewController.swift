import UIKit
import Firebase
import Kingfisher
import ProgressHUD

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Auth.auth().currentUser != nil {
            Api.User.isOnline(bool: true)
            self.tabBarController?.tabBar.isHidden = true
            let mainVC = MainViewController()
            self.navigationController?.pushViewController(mainVC, animated: true)
        }
        else {
            let main = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WelcomeViewController")
            self.navigationController?.pushViewController(main, animated: true)
        }
        
    }
}

