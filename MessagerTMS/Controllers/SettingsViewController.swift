import UIKit
import Firebase
import Kingfisher

class SettingsViewController : UIViewController {
    @IBOutlet var logOutButton: UIButton!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    
    var username: String?
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Settings"
        navigationController?.navigationBar.prefersLargeTitles = true
        logOutButton.layer.cornerRadius = 21
        observeData()
    }
    
    func observeData() {
        Api.User.getUserInforSingleEvent(uid: Auth.auth().currentUser!.uid) { [weak self] (user) in
            guard let self = self else {return}
            self.usernameLabel.text = user.username
            self.emailLabel.text = user.email
            let url = URL(string: user.profileImageUrl)
            self.avatarImageView.kf.setImage(with: url)
        }
    }
    
    @IBAction func logOutClicked(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        Api.User.isOnline(bool: false)
        try! Auth.auth().signOut()
        let main = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WelcomeViewController")
        self.navigationController?.pushViewController(main, animated: true)
    }
}

extension SettingsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageSelected = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            image = imageSelected
            avatarImageView.image = imageSelected
        }
        
        if let imageOriginal = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = imageOriginal
            avatarImageView.image = imageOriginal
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
}

