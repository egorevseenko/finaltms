import UIKit

class WelcomeViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func createAnAccountClicked(_ sender: Any) {
        let secondVC = RegViewController()    
        navigationController?.pushViewController(secondVC, animated: true)
        
    }
    @IBAction func loginClicked(_ sender: Any) {
        let secondVC = LoginViewController()
        navigationController?.pushViewController(secondVC, animated: true)
    }
}

