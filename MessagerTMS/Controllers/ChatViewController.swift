import UIKit
import Kingfisher
import Firebase
import FirebaseAuth
import MobileCoreServices
import AVFoundation
import ProgressHUD

class ChatViewController: UIViewController, UITextViewDelegate{
    var user: User?
    var messages: [Message] = []
    var picker = UIImagePickerController()
    var isActive = false
    var lastTimeOnline = ""
    
    var imagePartner: UIImage!
    @IBOutlet var textView: UITextView!
    @IBOutlet var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var inputViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var chatTableViewBottomConstraint: NSLayoutConstraint!
    
    
    //MARK:- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        messageTextView.layer.cornerRadius = 13
        observeActivity()
        
        chatTableView.tableFooterView = UIView()
        chatTableView.separatorStyle = .none
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.transform = CGAffineTransform(rotationAngle: -(CGFloat)(Double.pi));
        picker.delegate = self
        let chatViewCell =  UINib(nibName: "ChatMessageCellTableViewCell", bundle: nil)
        chatTableView.register(chatViewCell, forCellReuseIdentifier: "ChatMessageCellTableViewCell")
        chatTableView.tableFooterView = UIView()
        messages.reverse()
        setupPicker()
        
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(firstRecognizerClicked(_:)))
        view.addGestureRecognizer(tapRecognizer)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        
//        navigationBarSetInfo(bool: false)
        observeMessages()
        observeActivity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
        //        Database.database().reference().removeAllObservers()
    }
    
    
    @IBAction func firstRecognizerClicked(_ sender: Any) {
        view.endEditing(true)
    }
    
    func setupPicker(){
        picker.delegate = self
    }
    
    //MARK:- Keyboard
    
    func textViewDidChange(_ textView: UITextView){
        let size = CGSize(width: textView.frame.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        
        textView.constraints.forEach { (constraint) in
            if constraint.firstAttribute == .height {
                print(estimatedSize.height)
                if estimatedSize.height > 150{
                    constraint.constant = 150
                    viewHeightConstraint.constant = 170
                }else {
                    constraint.constant = estimatedSize.height
                    viewHeightConstraint.constant = estimatedSize.height + 20
                }
            }
        }
        textView.setNeedsUpdateConstraints()
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let screenHeight = UIScreen.main.bounds.size.height
        var keyboardHeight: CGFloat?
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
        }
        
        if notification.name == UIResponder.keyboardWillHideNotification{
            inputViewBottomConstraint.constant = 0
            chatTableViewBottomConstraint.constant = 0
        } else if let keyboardHeight = keyboardHeight {
            inputViewBottomConstraint.constant = -keyboardHeight  + view.safeAreaInsets.bottom
        }
        view.layoutIfNeeded()
    }
    
    func navigationBarSetInfo(bool: Bool){
        
        var status = ""
        var color = UIColor()
        if bool {
            status = "Online"
            color = UIColor.green
        } else {
            status = self.lastTimeOnline
            color = UIColor.red
        }
        
        let avatarImageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        let infoLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        let url = URL(string: user!.profileImageUrl)
        avatarImageView.kf.setImage(with: url)
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.layer.cornerRadius = 18
        avatarImageView.clipsToBounds = true
        containView.addSubview(avatarImageView)
        
        let rightBarItem = UIBarButtonItem(customView: containView)
        self.navigationItem.rightBarButtonItem = rightBarItem
        
        let attributed = NSMutableAttributedString(string: user!.username + "\n" , attributes: [.font : UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.black])
        
        attributed.append(NSAttributedString(string: status, attributes: [.font : UIFont.systemFont(ofSize: 13), .foregroundColor: UIColor.black]))
        infoLabel.attributedText = attributed
        infoLabel.textAlignment = .center
        infoLabel.numberOfLines = 0
        self.navigationItem.titleView = infoLabel
    }
    
    
    @IBAction func sendMessageClicked(_ sender: Any) {
        if let text = messageTextView.text, text != ""{
            messageTextView.text = ""
            sendToFirebase(dict: ["text": text as Any])
            viewHeightConstraint.constant = 50
            textViewHeightConstraint.constant = 32.5
        }
        view.layoutIfNeeded()
    }
    
    
    @objc func keyboardDidShow(_ notification: Notification) {
        
    }
    
    
    //MARK:- Send to Firebase
    
    func sendToFirebase(dict: Dictionary<String, Any>){
        let date = Date().timeIntervalSince1970
        var value = dict
        value["from"] = Auth.auth().currentUser?.uid
        value["to"] = user?.uid
        value["date"] = date
        value["read"] = true
        
        Api.Message.sendMessage(from: Auth.auth().currentUser!.uid, to: user!.uid, value: value)
    }
    
    func observeMessages(){
        guard let user = user else { return }
        Api.Message.receiveMessage(from: Auth.auth().currentUser!.uid, to: user.uid) { (message) in
            self.messages.append(message)
            self.sortMessages()
        }
        Api.Message.receiveMessage(from: user.uid, to: Auth.auth().currentUser!.uid) { (message) in
            self.messages.append(message)
            self.sortMessages()
        }
    }
    
    func sortMessages(){
        messages = messages.sorted(by: {$0.date > $1.date})
        self.chatTableView.reloadData()
    }
    
    @IBAction func mediaButtonClicked(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        let camera = UIAlertAction(title: "Take a photo", style: .default) { [self]  (_)  in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                    self.picker.sourceType = .camera
//                    self.picker.mediaTypes = [String(kUTTypeImage), String(kUTTypeMovie)]
                    self.picker.modalPresentationStyle = .fullScreen
                    self.present(picker, animated: true, completion: nil)
                    self.modalPresentationStyle = .fullScreen
                }
            }
        }
        
        let video = UIAlertAction(title: "Take a video", style: .default) { [self]  (_)  in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                    self.picker.sourceType = .camera
                    self.picker.mediaTypes = [String(kUTTypeMovie)]
                    self.picker.videoExportPreset = AVAssetExportPresetPassthrough
                    self.picker.videoMaximumDuration = 60
                    self.picker.modalPresentationStyle = .fullScreen
                    self.present(picker, animated: true, completion: nil)
                }
            }
        }
        
        let library = UIAlertAction(title: "Choose an image or a video", style: .default) {[self]  (_)  in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.picker.sourceType = .photoLibrary
                self.picker.mediaTypes = [String(kUTTypeImage), String(kUTTypeMovie)]
                self.picker.modalPresentationStyle = .fullScreen
                self.present(picker, animated: true, completion: nil)
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(camera)
        alert.addAction(video)
        alert.addAction(library)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func observeActivity() {
        guard let user = user else { return }
        let ref =  Database.database().reference().child("users").child(user.uid).child("isOnline")//.child("online")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if let snap = snapshot.value as? Dictionary<String, Any> {
                if let active = snap["online"] as? Bool {
                    self.isActive = active
                }
                
                if let latest = snap["latest"] as? Double {
                    self.lastTimeOnline = latest.convertDate()
                }
            }
            
            self.navigationBarSetInfo(bool: self.isActive)
        }
        ref.observe(.childChanged) { (snapshot) in
            if let snap = snapshot.value {
                if snapshot.key == "online" {
                    self.isActive = snap as! Bool
                }
                if snapshot.key == "latest" {
                    let latest = snap as! Double
                    self.lastTimeOnline = latest.convertDate()
                }
                
                self.navigationBarSetInfo(bool: self.isActive)
            }
        }
    }
}

//MARK:- TableView

extension ChatViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMessageCellTableViewCell") as! ChatMessageCellTableViewCell
        cell.selectionStyle = .none
        cell.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi));
        cell.configureCell(uid: Auth.auth().currentUser!.uid, message: messages[indexPath.row ])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        let message = messages[indexPath.row]
        let text = message.text
        if !text.isEmpty {
            height = text.estimateFrameForText(text).height + 60
        }
        
        let heightMessage = message.height
        let widthMessage = message.width
        if heightMessage != 0, widthMessage != 0 {
            height = CGFloat(heightMessage / widthMessage * 250)
        }
        return height
    }
}


//MARK: - Picker


extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL{
            handleSelectedForUrl(videoUrl)
        } else {
            handleSelectedForInfo(info)
        }
    }
    
    func handleSelectedForUrl(_ url: URL){
        //save video data
    }
    
    func handleSelectedForInfo(_ info: [UIImagePickerController.InfoKey : Any]){
        var selectedImageFromPicker: UIImage?
        if let imageSelected = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            selectedImageFromPicker = imageSelected
        }
        if let imageOriginal = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            selectedImageFromPicker = imageOriginal
        }
        //save photo data
        
        let imageName = NSUUID().uuidString
        StorageService.savePhotoMessage(image: selectedImageFromPicker, id: imageName, onSuccess: { (anyValue) in
            if let dict = anyValue as? [String: Any] {
                self.sendToFirebase(dict: dict)
            }
        }) { (errorMessage) in
            
        }
        
        self.picker.dismiss(animated: true, completion: nil)
    }
}
