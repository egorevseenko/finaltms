import UIKit
import Firebase

class VerifyPinViewController: UIViewController {
    var verification_ID: String?
    @IBOutlet weak var verificationTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func submitClicked(_ sender: Any) {
        if let verification_ID = verification_ID,let verification = verificationTextField.text{
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification_ID ?? "",
                                                                     verificationCode: verification)
            Auth.auth().signIn(with: credential) { authData, error in
                if (error) != nil {
                    return
                }
                else {
//                    let secondVC = MainViewController()
//                    self.navigationController?.pushViewController(secondVC, animated: true)
                    let tabBarVC = UITabBarController()
                    
                    let vc1 = FirstViewController()
                    let vc2 = SecondViewController()
                    let vc3 = ThirdViewController()
                    
                    tabBarVC.setViewControllers([vc1, vc2, vc3], animated: false)
                    tabBarVC.modalPresentationStyle = .fullScreen
                    self.present(tabBarVC, animated: true)
                }
            }
        }
    }
}

class FirstViewController : UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGreen
    }
}

class SecondViewController : UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemRed
    }
}

class ThirdViewController : UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBlue
    }
}
