import UIKit
import Firebase

class MessageTableViewCell: UITableViewCell {
    
    
    var user: User!
    var inbox: Inbox!
    var controller: MessageViewController!
    var inboxChangedMessageHandle: DatabaseHandle!
    var inboxChangedOnlineHandle: DatabaseHandle!
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var onlineView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.cornerRadius = 30
        onlineView.backgroundColor = .systemGreen
//        self.onlineView.isHidden == true
    }
    
    func observeActivity() {
        let ref =  Database.database().reference().child("users").child(user.uid).child("isOnline")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if let snap = snapshot.value as? Dictionary<String, Any> {
                if let active = snap["online"] as? Bool {
                    self.onlineView.isHidden != active
                }
            }
        }
        ref.observe(.childChanged) { (snapshot) in
            if let snap = snapshot.value {
                if snapshot.key == "online" {
                    if snap as! Bool{
                        self.onlineView.isHidden = false
                    }
                    else {
                        self.onlineView.isHidden = true
                    }
                }
            }
        }
    }
    
    func updateInboxMessage(){
        let refInbox = inbox.databaseInboxInfor(from: Auth.auth().currentUser!.uid, to: inbox.user.uid)
        if inboxChangedMessageHandle != nil{
            refInbox.removeObserver(withHandle: inboxChangedMessageHandle)
        }
        
        
        inboxChangedMessageHandle = refInbox.observe(.childChanged, with: { (snapshot) in
            if let snap = snapshot.value {
                self.inbox.updateData(key: snapshot.key, value: snap)
                self.controller.sortedInbox()
            }
        })
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        let refInbox = inbox.databaseInboxInfor(from: Auth.auth().currentUser!.uid, to: inbox.user.uid)
        if inboxChangedMessageHandle != nil {
            refInbox.removeObserver(withHandle: inboxChangedMessageHandle)
        }
    }
}




































//if inboxChangedOnlineHandle != nil {
//    ref.removeObserver(withHandle: inboxChangedOnlineHandle)
//}
//
//inboxChangedOnlineHandle = ref.observe(.childChanged) { (snapshot) in
//    if let snap = snapshot.value {
//        if snapshot.key == "online" {
//            self.onlineView.isHidden = (snap as! Bool) == true ? false : true
//        }
//    }
//}
