import UIKit
import Kingfisher
import AVFoundation

class ChatMessageCellTableViewCell: UITableViewCell {
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var textMessageLabel: UILabel!
    @IBOutlet weak var photoMessageImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var widthConstrain: NSLayoutConstraint!
    @IBOutlet weak var messageViewLeftConstrain: NSLayoutConstraint!
    @IBOutlet weak var messageViewRightConstrain: NSLayoutConstraint!
    
    var message: Message!
    
    override func awakeFromNib() {
        super.awakeFromNib() 
        messageView.clipsToBounds = true
        messageView.clipsToBounds = true
        messageView.layer.borderWidth = 0.4
        photoMessageImageView.layer.cornerRadius = 15
        photoMessageImageView.clipsToBounds = true
        
        photoMessageImageView.isHidden = true
        textMessageLabel.isHidden = true 
    }
    
    override func prepareForReuse() {  // изменяем размер ячейки TableView
        super.prepareForReuse()
        photoMessageImageView.isHidden = true
        textMessageLabel.isHidden = true
    }
    
    func configureCell(uid: String, message: Message){
        self.message = message
        let text = message.text
        if !text.isEmpty{
            textMessageLabel.isHidden = false
            textMessageLabel.text = message.text
            
            let widthValue = text.estimateFrameForText(text).width + 50
            
            if widthValue < 85 {
                widthConstrain.constant = 85
            } else {
                widthConstrain.constant = widthValue
            }
            dateLabel.textColor = .lightGray
        } else {
            photoMessageImageView.isHidden = false
            let urlImage = URL(string: message.imageUrl)
            photoMessageImageView.kf.setImage(with: urlImage)
            photoMessageImageView.contentMode = .scaleToFill
            messageView.layer.borderColor = UIColor.clear.cgColor
//            messageView.contentMode = UIView.ContentMode.scaleAspectFit
            widthConstrain.constant = 250
            dateLabel.textColor = .white

            
        }
        
        if uid == message.from{
            messageView.backgroundColor = .groupTableViewBackground
            messageView.layer.borderColor = UIColor.clear.cgColor
            messageViewRightConstrain.constant = 8
            messageViewLeftConstrain.constant = UIScreen.main.bounds.width - messageViewRightConstrain.constant - widthConstrain.constant
        }
        else {
            messageView.backgroundColor = .white
            messageView.layer.borderColor = UIColor.lightGray.cgColor
            messageViewLeftConstrain.constant = 8
            messageViewRightConstrain.constant = UIScreen.main.bounds.width - messageViewLeftConstrain.constant - widthConstrain.constant
        }
        let date = Date(timeIntervalSince1970: message.date)
        let dateString = timeAgoSinceDate(date, currentDate: Date(), numericDates: true)
        dateLabel.text = dateString
        self.formatHeaderTimeLabel(time: date) { (text) in
            self.dateLabel.text = text
        }
        
    }
    
    func formatHeaderTimeLabel(time: Date, completion: @escaping (String) -> ()) {
        var text = ""
        let currentDate = Date()
        let currentDateString = currentDate.toString(dateFormat: "yyyyMMdd")
        let pastDateString = time.toString(dateFormat: "yyyyMMdd")
        print(currentDateString)
        print(pastDateString)
        if pastDateString.elementsEqual(currentDateString) == true {
            text = time.toString(dateFormat: "HH:mm a") + ", Today"
        } else {
            text = time.toString(dateFormat: "dd/MM/yyyy")
        }
        
        completion(text)
    }
}
