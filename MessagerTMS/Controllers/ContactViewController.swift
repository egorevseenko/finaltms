import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import Kingfisher

class ContactViewController : UIViewController, UISearchResultsUpdating{
//    @IBOutlet weak var contactTableView: UITableView!
    var contactTableView: UITableView!
    var users: [User] = []
    var searchResult: [User] = []
    var searchController: UISearchController = UISearchController(searchResultsController: nil)
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Contacts"
        view.backgroundColor = .white
        navigationController?.navigationBar.prefersLargeTitles = true
        setupSearchBarController()
        setupTableView()
        contactTableView.tableHeaderView = searchController.searchBar
        let searchedText = searchController.searchBar.text
        searchController.isActive = false
        searchController.searchBar.text = searchedText
        registrationOfTableView()
        navigationController?.navigationBar.backgroundColor = .white
//        navigationController?.navigationBar.bac
//        searchController.searchBar.backgroundImage = UIImage()
        loadDataFromFirebase()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Database.database().reference().removeAllObservers()
    }
    func setupTableView() {
        contactTableView = UITableView(frame: .zero, style: .plain)
        contactTableView.translatesAutoresizingMaskIntoConstraints = false
        contactTableView.delegate = self
        contactTableView.dataSource = self
        view.addSubview(contactTableView)
        contactTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        contactTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        contactTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        contactTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    }
    func registrationOfTableView(){
        contactTableView.tableFooterView = UIView()
        let contactViewCell = UINib(nibName: "ContactTableViewCell", bundle: nil)
        contactTableView.register(contactViewCell, forCellReuseIdentifier: "ContactTableViewCell")
    }
    
    
    //MARK:- Firebase
    
    func loadDataFromFirebase(){
        //при первой загрузке ContactViewController обновляется число пользователей в таблице.Создаем наблюдателя который проходится по каждому из уникальных номеров пользователя,которые храняться в "users" в БД на firebase  и приобразует его данные в объкты класса User и добавляет каждого пользователя в массив users
        Database.database().reference().child("users").observe(.childAdded){ [self] (snapshot) in
            if let dict = snapshot.value as? Dictionary<String,Any>{
                if let user = User.transformUser(dict: dict){
                    self.users.append(user)
                }
                sortContacts()
                self.contactTableView.reloadData()
            }
        }
    }
    
    //MARK:- SeachBar
    
    func setupSearchBarController(){
        searchController.searchResultsUpdater = self
//        searchController.obscuresBackgroundDuringPresentation = false //позволяет взаимодействовать с отображаемым контентом
        searchController.searchBar.placeholder = "Search users..."
        searchController.searchBar.barTintColor = .white
//        definesPresentationContext = true //позволяет отпустить строку поиска при переходе на другой экран
        navigationItem.hidesSearchBarWhenScrolling = true
       
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text!.isEmpty{
            searchResult = []
            view.endEditing(true)
        } else {
            if let textLowercased = searchController.searchBar.text?.lowercased(){
                filterContent(for: textLowercased)
            }
        }
        self.contactTableView.reloadData()
    }
    
    func filterContent(for searchText: String){
        searchResult = self.users.filter {
            return $0.username.lowercased().range(of: searchText) != nil
        }
    }
    func sortContacts(){
        users = users.sorted(by: {$0.username < $1.username})
        self.contactTableView.reloadData()
    }
    
}

extension ContactViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive{
            return searchResult.count
        } else {
            return self.users.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatVC = ChatViewController()
        chatVC.user = users[indexPath.row]
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell") as! ContactTableViewCell
        let user =  searchController.isActive ? searchResult[indexPath.row] : users[indexPath.row]
        let url = URL(string: user.profileImageUrl)
        cell.avatarImageView.kf.setImage(with: url)
        cell.usernameLabel.text = user.username
        cell.selectionStyle = .none
        return cell
    }
}
