import UIKit

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabBarVC = UITabBarController()
        
        let contactVC = UINavigationController(rootViewController: ContactViewController())
        let messageVC = UINavigationController(rootViewController: MessageViewController())
        let settingsVC = UINavigationController(rootViewController: SettingsViewController())
        
        contactVC.title = "Contacts"
        messageVC.title = "Messages"
        settingsVC.title = "Settings"
        
        tabBarVC.setViewControllers([contactVC, messageVC, settingsVC], animated: false)
        
        guard let items = tabBarVC.tabBar.items else {return}
        
        let images = ["person.crop.circle", "message", "gear"]
        
        for i in 0..<items.count{
            items[i].image = UIImage(systemName: images[i])
        }
        
        tabBarVC.modalPresentationStyle = .fullScreen
        self.present(tabBarVC, animated: true)
        tabBarVC.selectedIndex = 1
    }
}




