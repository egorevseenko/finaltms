import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import ProgressHUD

class MessageViewController : UIViewController{
    @IBOutlet weak var messageTableView: UITableView!
    var users: [User] = []
    var inboxArray: [Inbox] = []
    var lastInboxDate: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProgressHUD.show("Loading...")
        messageTableView.delegate = self
        messageTableView.dataSource = self
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(newMessageClicked(_:)))

        registrationOfTableView()
        observeMessages()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.messageTableView.reloadData()
        self.tabBarController?.tabBar.isHidden = false 
        }

    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        ProgressHUD.dismiss()
        self.messageTableView.reloadData()
    }
    
    @objc func newMessageClicked(_ sender: Any){
        let contactVC = ContactViewController()
        self.navigationController?.pushViewController(contactVC, animated: true)
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.tabBarController?.tabBar.isHidden = true
    }

    
    func registrationOfTableView(){
        title = "Messages"
        let messageViewCell = UINib(nibName: "MessageTableViewCell", bundle: nil)
        messageTableView.register(messageViewCell, forCellReuseIdentifier: "MessageTableViewCell")
        self.messageTableView.tableFooterView = UIView()
        self.messageTableView.backgroundColor = .white
        view.backgroundColor = .white
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func sortedInbox(){
        inboxArray = inboxArray.sorted(by: {$0.date > $1.date})
        lastInboxDate = inboxArray.last!.date
        DispatchQueue.main.async {
            self.messageTableView.reloadData()
        }
    }
    
    //MARK: - Firebase
    
    func observeMessages() {
        Api.Inbox.lastMessages(uid: Auth.auth().currentUser!.uid) { (inbox) in
            if !self.inboxArray.contains(where: { $0.user.uid == inbox.user.uid }){
                self.inboxArray.append(inbox)
                self.sortedInbox()
                ProgressHUD.dismiss()
            }
        }
    }
    
}


    //MARK: - TableView
extension MessageViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.inboxArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatVC = ChatViewController()
        chatVC.user = inboxArray[indexPath.row].user
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell") as! MessageTableViewCell
        cell.selectionStyle = .none
        let inbox = inboxArray[indexPath.row]
        let url = URL(string: inbox.user.profileImageUrl)
        cell.avatarImageView.kf.setImage(with: url)
        cell.usernameLabel.text = inbox.user.username
//        cell.lastMessageLabel.text = inbox.text
        if !inbox.text.isEmpty {
            cell.lastMessageLabel.text = inbox.text
        } else {
            cell.lastMessageLabel.text = "MEDIA"
        }
        cell.inbox = inboxArray[indexPath.row]
        cell.user = inboxArray[indexPath.row].user
        cell.controller = self
//        cell.onlineView.isHidden == true
        cell.observeActivity()
        cell.updateInboxMessage()
        return cell
    }
}

