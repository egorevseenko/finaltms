import UIKit
import Firebase

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var phoneTextField: UITextField!
    var verification_ID: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func secondClicked(_ sender: Any) {
        if let phoneNumber = phoneTextField.text{
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                if let error = error{
                    return
                }
                else{
                    self.verification_ID = verificationID
                    let secondVC = VerifyPinViewController()
                    secondVC.verification_ID = verificationID
                    self.navigationController?.pushViewController(secondVC, animated: true)
                }
            }
        }
    }
}
